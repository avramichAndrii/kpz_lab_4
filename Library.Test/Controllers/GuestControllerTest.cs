﻿using Library.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Library.Test.Controllers
{
    [TestClass]
    public class GuestControllerTest
    {
        [TestMethod]
        public void Guest()
        {
            GuestController controller = new GuestController();

            // Act
            var result = controller.Guest();

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
