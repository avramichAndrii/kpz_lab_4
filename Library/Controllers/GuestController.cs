﻿using Library.BLL.Services;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Controllers
{
    /// <summary>
    /// Comment controller that displays users comments
    /// </summary>
    public class GuestController : Controller
    {
        private readonly CommentService _commentService;

        public GuestController()
        {
            _commentService = new CommentService();
        }

        [HttpGet]
        public async Task<ActionResult> Guest()
        {
            List<CommentView> views = new List<CommentView>();
            foreach(var i in await _commentService.GetAllAsync())
            {
                views.Add(i.ToView());
            }
            ViewBag.Comments = views;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddComment(string name, string comment)
        {
            CommentView commentToAdd = new CommentView()
            {
                User = name,
                Body = comment,
                Date = DateTime.Now
            };
            await _commentService.CreateAsync(commentToAdd.FromView());
            return Redirect("https://localhost:44333/Guest/Guest");
        }
    }
}