﻿using System;
using System.Web.Mvc;

namespace Library.Controllers
{
    /// <summary>
    /// Controller that redirects 404 error
    /// </summary>
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }
    }
}