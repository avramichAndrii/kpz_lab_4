﻿using Library.BLL.Services;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Controllers
{
    /// <summary>
    /// Main controller that displays articles
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ArticleService _articleService;

        public HomeController()
        {
            _articleService = new ArticleService();
        }
        [HttpGet]
        public async Task<ActionResult> Main()
        {
            List<ArticleView> articles = new List<ArticleView>();
            foreach (var i in await _articleService.GetAllAsync())
            {
                articles.Add(i.ToView());
            }
            ViewBag.Articles = articles;
            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Store(string name, string surname, string txt)
        {
            ArticleView article = new ArticleView();
            article.Name = name;
            article.Author = surname;
            article.Description = txt;
            article.Date = DateTime.Now;
            await _articleService.CreateAsync(article.FromView());
            return Redirect("~/Home/Main");
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(Guid id)
        {
            await _articleService.DeleteAsync(id);
            return Redirect("~/Home/Main");
        }
    }

}