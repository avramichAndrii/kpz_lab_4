﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Helpers
{
    /// <summary>
    /// Select helper with user marks
    /// </summary>
    public static class Helpers
    {
        public static MvcHtmlString CreateSelect(this HtmlHelper html, string[] items)
        {
            TagBuilder select = new TagBuilder("select");
            select.Attributes.Add("name", "opt");
            foreach (string item in items)
            {
                TagBuilder option = new TagBuilder("option");
                option.SetInnerText(item);
                select.InnerHtml += option.ToString();
            }
            return new MvcHtmlString(select.ToString());
        }
    }
}