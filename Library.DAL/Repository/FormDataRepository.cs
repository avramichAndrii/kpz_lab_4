﻿using Library.DAL.Exceptions;
using Library.DAL.Interfaces;
using Library.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    /// <summary>
    /// Repository for data from forms
    /// </summary>
    public class FormDataRepository : IRepository<FormData>
    {
        private readonly LibraryContext _libraryContext;

        public FormDataRepository()
        {
            _libraryContext = new LibraryContext();

        }
        /// <summary>
        /// Creates new data model
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task CreateAsync(FormData data)
        {
            try
            {
                _libraryContext.FormData.Add(data);
                await _libraryContext.SaveChangesAsync();
            }
            catch
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Deletes datamodel
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            FormData data = await _libraryContext.FormData.SingleOrDefaultAsync(x => x.Id == id);
            if (data != null)
            {
                try
                {
                    _libraryContext.FormData.Remove(data);
                }
                catch
                {
                    throw new InvalidDomainOperationException();
                }
            }
        }
        /// <summary>
        /// Gets all data models 
        /// </summary>
        /// <returns></returns>
        public async Task<IList<FormData>> GetAllAsync()
        {
            return await _libraryContext.FormData.ToListAsync();
        }
        /// <summary>
        /// Gets data model by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<FormData> GetAsync(Guid id)
        {
            try
            {
                return await _libraryContext.FormData.SingleOrDefaultAsync(x => x.Id == id);
            }
            catch
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Updates existing data model
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<FormData> UpdateAsync(Guid id, FormData data)
        {
            FormData modelToUpdate = await _libraryContext.FormData.SingleOrDefaultAsync(cm => cm.Id == id);
            if (modelToUpdate == null)
            {
                throw new DomainNotFoundException(id);
            }
            modelToUpdate.Name = data.Name;
            modelToUpdate.Description = data.Description;
            modelToUpdate.Gender = data.Gender;
            modelToUpdate.Mark = data.Mark;
            modelToUpdate.Surname = data.Surname;
            try
            {
                await _libraryContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDomainOperationException();
            }
        }
    }
}
