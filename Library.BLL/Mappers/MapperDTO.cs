﻿using Library.BLL.ModelsDTO;
using Library.DAL.Models;


namespace Library.BLL.Mappers
{
    /// <summary>
    /// Mapper class that uses extension methods
    /// </summary>
    public static class MapperDTO
    {
        public static ArticleDTO ToDTO(this Article article)
        {
            return new ArticleDTO()
            {
                Id = article.Id,
                Author = article.Author,
                Date = article.Date,
                Description = article.Description,
                Image = article.Image,
                Name = article.Name
            };
        }

        public static Article FromDTO(this ArticleDTO article)
        {
            return new Article()
            {
                Id = article.Id,
                Author = article.Author,
                Date = article.Date,
                Description = article.Description,
                Image = article.Image,
                Name = article.Name
            };
        }

        public static CommentDTO ToDTO(this Comment comment)
        {
            return new CommentDTO()
            {
                Id = comment.Id,
                Date = comment.Date,
                User = comment.User,
                Name = comment.Name,
                Body = comment.Body
            };
        }

        public static Comment FromDTO(this CommentDTO comment)
        {
            return new Comment()
            {
                Id = comment.Id,
                Date = comment.Date,
                User = comment.User,
                Name = comment.Name,
                Body = comment.Body
            };
        }

        public static FormDataDTO ToDTO(this FormData data)
        {
            return new FormDataDTO()
            {
                Description = data.Description,
                Gender = data.Gender,
                Mark = data.Mark,
                Name = data.Name,
                Surname = data.Surname
            };
        }

        public static FormData FromDTO(this FormDataDTO data)
        {
            return new FormData()
            {
                Description = data.Description,
                Gender = data.Gender,
                Mark = data.Mark,
                Name = data.Name,
                Surname = data.Surname
            };
        }
    }
}
