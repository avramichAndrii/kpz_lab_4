﻿using Library.BLL.Mappers;
using Library.BLL.ModelsDTO;
using Library.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    /// <summary>
    /// Service class for comments
    /// </summary>
    public class CommentService
    {
        private readonly CommentRepository _commentRepository;
        public CommentService()
        {
            _commentRepository = new CommentRepository();
        }

        public async Task<IList<CommentDTO>> GetAllAsync()
        {
            List<CommentDTO> comments = new List<CommentDTO>();
            foreach (var i in await _commentRepository.GetAllAsync())
            {
                comments.Add(i.ToDTO());
            }
            return comments;
        }

        public async Task<CommentDTO> GetArticleAsync(Guid id)
        {
            var tmp = await _commentRepository.GetAsync(id);
            return tmp.ToDTO();
        }

        public async Task CreateAsync(CommentDTO comment)
        {
            await _commentRepository.CreateAsync(comment.FromDTO());
        }

        public async Task<CommentDTO> UpdateAsync(Guid id, CommentDTO comment)
        {
            var tmp = await _commentRepository.UpdateAsync(id, comment.FromDTO());
            return tmp.ToDTO();
        }

        public async Task DeleteAsync(Guid id)
        {
            await _commentRepository.DeleteAsync(id);
        }
    }
}
